variable "stack_id" {
  default = "network"
}

variable "routing_mode" {
  default = "REGIONAL"
}

variable "auto_create_subnetworks" {
  default = "false"
}

variable "subnets" {
  default = []
  type    = "list"
}

resource "google_compute_network" "network" {
  name                    = "${var.stack_id}"
  auto_create_subnetworks = "${var.auto_create_subnetworks}"
  routing_mode            = "${var.routing_mode}"
}
