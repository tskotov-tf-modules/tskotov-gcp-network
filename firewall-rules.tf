resource "google_compute_subnetwork" "subnetwork" {
  ip_cidr_range = "${lookup(var.subnets[count.index], "cidr")}"
  name          = "${var.stack_id}-${count.index}"
  network       = "${google_compute_network.network.self_link}"
  region        = "${lookup(var.subnets[count.index], "region")}"

  count = "${length(var.subnets)}"
}

resource "google_compute_firewall" "allow_internal" {
  description   = "Allow all internal communication between VM instances"
  name          = "${var.stack_id}-allow-internal"
  network       = "${google_compute_network.network.self_link}"
  source_ranges = ["10.0.0.0/8", "172.16.0.0/12", "192.168.0.0/16"]
  priority      = "65534"

  allow {
    protocol = "all"
  }
}

resource "google_compute_firewall" "allow_ssh" {
  name     = "${var.stack_id}-allow-ssh"
  network  = "${google_compute_network.network.self_link}"
  priority = "65534"

  allow {
    protocol = "tcp"
    ports    = ["22"]
  }
}

resource "google_compute_firewall" "allow_http" {
  name     = "${var.stack_id}-allow-http"
  network  = "${google_compute_network.network.self_link}"
  priority = "65534"

  allow {
    protocol = "tcp"
    ports    = ["80"]
  }

  target_tags = ["http"]
}

resource "google_compute_firewall" "allow_https" {
  name     = "${var.stack_id}-allow-https"
  network  = "${google_compute_network.network.self_link}"
  priority = "65534"

  allow {
    protocol = "tcp"
    ports    = ["443"]
  }

  target_tags = ["https"]
}
