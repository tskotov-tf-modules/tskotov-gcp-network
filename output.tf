output "network-cidrs" {
  value = ["${google_compute_subnetwork.subnetwork.*.ip_cidr_range}"]
}

output "network-regions" {
  value = ["${google_compute_subnetwork.subnetwork.*.region}"]
}

output "networks" {
  value = ["${google_compute_subnetwork.subnetwork.*.self_link}"]
}